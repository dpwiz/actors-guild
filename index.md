# OCap layer for Haskell Actor Library

## Summary

This project aims to develop network/IPC capabilities for the a Haskell actor library `troupe`, using ocap-based protocols.
This would enable modern, secure, and efficient communication in distributed systems.

The protocols are OCapN and Syndicate - both related to CapTP, but different in focus (RPC vs sharing state).

The result will be a set of packages for protocol implementations, their integrations into the actor framework, and a set of demonstration environments.
To get more momentum behind the project we will provide starter projects and guix/nix environments.
Finally, we want to prepare to conduct a workshop on making distributed applications.

## Context

Three evolving technologies can contribute to the development of the Haskell actor library:

- [Troupe]: A new iteration of the actor framework for Haskell. The previous attempt in form of [Cloud Haskell] had a well-documented model and achieved production use, but the consensus was that the resulting system ended up too complex. The focus now lies on intra-process organization, leaving network communication out of scope. This is where we pick up and provide the complementary part.
- [Spritely]: Inter-actor communication based on object capabilities, designed for a wide ecosystem of services, and a continuation of the CapTP protocol/framework.
- [Syndicate]: Also, an ocap-based actor framework, but with primary concern in task-oriented data sharing.

[Troupe]: https://github.com/NicolasT/troupe/
[Cloud Haskell]: https://haskell-distributed.github.io/
[Spritely]: https://spritely.institute/files/docs/guile-goblins/0.10/OCapN-The-Object-Capabilities-Network.html
[Syndicate]: https://syndicate-lang.org/about/

## Project Opportunity

We propose developing support libraries for OCapN and Syndicate protocols, then integrating them with the Troupe library, thus allowing actors to cooperatively solve tasks in a wider environment.
Implementing both Spritely and Syndicate simultaneously will bridge the gap between action and knowledge networks.

## Expected Benefits

- **Haskell developers** will gain access to a modern actor framework with secure RPC.
- **Spritely** will receive a new, parallel implementation in another language, providing an opportunity to verify protocol descriptions, compatibility, and community mechanisms.
- **Syndicate** will allow a direct comparison of the two approaches, their strengths, and weaknesses. A simultaneous implementation may reveal opportunities for unification into a single standard.
- **Application developers** will have a unified local/remote actor model, with a multilingual ecosystem of services, built-in PubSub without brokers, and a widely applicable network model.
- **Users** will benefit from trustworthy network communication implementation, applications working in Friend-to-Friend networks, and other mesh networks and applications that can discover their instances and synchronize.

## Expected Results

- A set of [packages] implementing the protocols and integrating them with Troupe.
- Test bench for the Haskell parts.
- Demo environment for working with Spritely.
- Demo environment for working with Syndicate.
- A cookbook for actor-based applications.
- A workshop covering the entire project.

[packages]: https://hackage.haskell.org/

## Why Us?

We have a serendipitous combination of experience for the project: having developed and deployed Cloud Haskell products, implemented libraries and tools for bridging the GNUnet services, and participated in a ocap-based microkernel OS development.
Also, we are huge fans of P2P building blocks like CRDTs and mesh networks.
