## Budget

The full set of deliverables has 9 chunks of roughly equivalent size (95% confidence interval of 7 to 30 days, mean=16, lognormal):
- Spritely track:
  * bindings
  * actors
  * demo
- Syndicate track: (same)
  * bindings
  * actors
  * demo
- Hybrid:
  * bridge demo
- Workshop track:
  * starter projects
  * workshop preparations

A two-person team working effective 4 hours at 35/h rate would hit all the goals with 95% confidence in 180 days.

The chunks contain basic components of coding, infrastructure and documentation that can be done in parallel, as do some of the chunks. This will smooth out unexpected problems (and boons).

[Estimates](./work-plan.squiggle)

## Compare

The previous major attempt at doing that, Cloud Haskell, attempted to do actor and network parts both.
This worked and was an academic success, but the developers felt it would be better to have independent libraries for those aspects. The actor part ended up in the `troupe` library, while we want to do the IPC now.
Also, the framework had a concept of pluggable transports which is reasonably successful by itself. However it got entangled with the actor framework internals due to its feature of "sending" code closures on the wire.

There are GNUnet bindings, but they had failed to gain traction. Partially for targeting an complex platform of C services, and in part for non-trivial integration of event loops.

The clean separation of concerns between actor and transport parts should alleviate some of those problems.

There are a lots of ad hoc solutions that start with a TCP connection, throwing a serialization layer on it and arriving at JSON-RPC or something like it. The fact there is software transactional memory in Haskell makes it more resilient to some of the typical errors, but the result is often fragile.

## Challenges

The main challenge we expect is tracking OCapN standardization efforts and adjusting our code to stay compatible. The Haskell is well-known to be resilient to that and pinning down key types would allow "move fast and fix things" workflow.
The working group acknowledges the protocol it is missing critical documentation at the moment, being implementation-defined right now. We will rely on our experience with Scheme-derived languages to recover it and upstream our docs if needed.

## Ecosystem

On the Haskell side, there are two package authors what we will be in contact with. One of the actor library, and another is the author of `preserves` library that is used by both Spritely and Syndicate protocols as a wire format (they are also a part of OCapN WG).

We will seek implementation help from the respective protocol authors. And then cross-promote inter-language demonstrations.

Supporting Spritely/OCapN is important given it is on track to form a next Fediverse protocol backbone.
Supporting Syndicate is important for its aptness for local/LAN/WAN configuration storage.

My teammate is hackspace host, so we can gather interested people for the workshop. Time permitting, we would want to do a workshop on one of the major Haskell hackathons (e.g. Munihac in october).
And I am promoting game development in Haskell and such a framework would be a blessing for all things multiplayer.
