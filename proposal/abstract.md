This project aims to develop network/IPC capabilities for the a Haskell actor library `troupe`, using ocap-based protocols.
This would enable modern, secure, and efficient communication in distributed systems.

The protocols are OCapN and Syndicate - both related to CapTP, but different in focus (RPC vs sharing state).

The result will be a set of packages for protocol implementations, their integrations into the actor framework, and a set of demonstration environments.
To get more momentum behind the project we will provide starter projects and guix/nix environments.
Finally, we want to prepare to conduct a workshop on making distributed applications.
